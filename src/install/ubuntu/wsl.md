# Enabling WSL and installing Ubuntu 
## Step 1 (Enabling + Downloading)
In order to enable WSL for Windows, first you must run the following command in **Windows Powershell** ran as **Administrator** (you can do this by right clicking on Powershell after searching it by pressing the Windows key)

	dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

![../../images/wsl-enabling.png](../../images/wsl-enabling.png)


Once the command has finished running, you must now restart your computer, you can do this by pressing **Windows Key + R** and running the following command.

	shutdown /r /t 0

Once your computer has restarted, now you can go to the Microsoft Store and download **Ubuntu** (not the 20.04 or the 18.04 LTS versions)

## Step 2 (Installing + Configuring)
Once you have downloaded the application **Ubuntu**, you can now run it to enter the first-time setup.

**Please note:** The first time setup may take a while, this is due to files being unpacked, once this has been done the application will launch quite quickly.

![../../images/wsl-configuration.png](../../images/wsl-configuration.png)

You should see the message 
> Installing, this may take a few minutes...

Wait until your prompt shows the message
> Enter a new UNIX username: 

This is where you type your account username that you use for Ubuntu, please note this username will appear before every command line and appear as the output for the `whoami` command.

After your type in your username, you will be prompted to enter a password.

**IMPORTANT:** You will **NOT** see your password as you are typing it in, so don't think that it is not working if you see nothing happening, this is just a feature of UNIX.

Type in a password that is associated with your UNIX account, please make this memorable as you will need to use it to run commands with escalated permissions (such as administrator aka root) (i.e. `sudo`).

Congratulations! Now you have successfully setup WSL and Ubuntu, please see the [post-install guide](../../post-install.md) in order to continue with your installation.
